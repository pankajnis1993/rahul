import React from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Toolbar from '@material-ui/core/Toolbar';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Fab from '@material-ui/core/Fab';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import FavoriteIcon from '@material-ui/icons/Favorite';
import NavigationIcon from '@material-ui/icons/Navigation';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerContainer: {
    overflow: 'auto',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  paper: {
    padding: '12px',
    boxShadow: 'none!important',
    borderRadius: '0!important',
    // borderBottom:'10px solid black',
    // padding: theme.spacing(2),
    // textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  right: {
    float: 'right'
  },
  Img: {
    width: '50px',
    marginRight: '20px',
    borderRadius: '50%',
    background: '#3f51b5'
  },
  border: {
    borderTop: '2px solid gray',
    // borderBottom: '2px solid gray'
  },
  responsive : {
    maxWidth: '100%',
    height: '300px',
    backgroundSize:'cover',
    width:'100%'
  },
  container:{
    marginBottom:'40px'
  }
}));

export default function Dashboard() {
  const classes = useStyles();

  return (
    <div className={classes.root}>


      <main className={classes.content}>
        {/* <Toolbar /> */}
        <Grid container  className={classes.container}>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <img src="/logo192.png" alt="logo" className={classes.Img} /> User Name
              <Fab color="primary" aria-label="edit" size="small" className={classes.right}>
                <MoreHorizIcon />
              </Fab>
            </Paper>
          </Grid>
          <Grid item xs={12}>
            <Paper className={`${classes.paper} ${classes.border}`}>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </Paper>
          </Grid>
          <Grid item xs={12}>
            <Paper className={`${classes.paper} ${classes.border}`}>
              <img  src="/234.jpg" alt="images" className={classes.responsive} />
            </Paper>
          </Grid>

          <Grid item xs={4}>
            <Paper className={classes.paper}>
              <Button variant="outlined" color="primary" fullWidth="true">
                Like
              </Button>
            </Paper>
          </Grid>
          <Grid item xs={4}>
            <Paper className={classes.paper}>
            <Button variant="outlined" color="primary" fullWidth="true">
                Comment
              </Button>
            </Paper>
          </Grid>
          <Grid item xs={4}>
            <Paper className={classes.paper}>
            <Button variant="outlined" color="primary" fullWidth="true">
                Share
              </Button>
            </Paper>
          </Grid>
        </Grid>

        {/* second  */}
        <Grid container className={classes.container} >
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <img src="/logo192.png" alt="logo" className={classes.Img} /> User Name
              <Fab color="primary" aria-label="edit" size="small" className={classes.right}>
                <MoreHorizIcon />
              </Fab>
            </Paper>
          </Grid>
          <Grid item xs={12}>
            <Paper className={`${classes.paper} ${classes.border}`}>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </Paper>
          </Grid>
          <Grid item xs={12}>
            <Paper className={`${classes.paper} ${classes.border}`}>
              <img  src="/234.jpg" alt="images" className={classes.responsive} />
            </Paper>
          </Grid>

          <Grid item xs={4}>
            <Paper className={classes.paper}>
              <Button variant="outlined" color="primary" fullWidth="true">
                Like
              </Button>
            </Paper>
          </Grid>
          <Grid item xs={4}>
            <Paper className={classes.paper}>
            <Button variant="outlined" color="primary" fullWidth="true">
                Comment
              </Button>
            </Paper>
          </Grid>
          <Grid item xs={4}>
            <Paper className={classes.paper}>
            <Button variant="outlined" color="primary" fullWidth="true">
                Share
              </Button>
            </Paper>
          </Grid>
        </Grid>

      </main>
    </div>
  );
}