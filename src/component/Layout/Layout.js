import React from 'react';
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";

import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';


// componet import
import RightSidebar from '../Layout/RightSidebar';
import Dashboard from '../pages/Dashboard';
import Sidebar from '../Layout/Sidebar';
import Orders from '../pages/Orders'
import Header from './Header';


const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
    },

}));


function Layout() {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Header />
            <CssBaseline />
            <Sidebar></Sidebar>
            <main className={classes.content}>
                <div className={classes.appBarSpacer} />
                <Router>
                    <Route exact path="/">
                        <Redirect to="/dashboard" />
                    </Route>
                    <Route exact path="/dashboard" component={Dashboard}></Route>
                    <Route exact path="/order" component={Orders}></Route>
                </Router>
            </main>
            <RightSidebar></RightSidebar>
        </div>
    )
}

export default Layout
